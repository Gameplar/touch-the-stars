﻿using System;
using UnityEngine;
using System.Collections;

public class SheepMover : MonoBehaviour
{

    private Vector3 _pos;
    // public Sky Sky;

    // Use this for initialization
    private void Start()
    {
    // Sky.MethodInvoke = EndTime;
    }

    public float SheepSpeed;

    // Update is called once per frame
    private void Update()
    {
        if (Sky.isStart > 0)
        {
            _pos = transform.position;

            transform.position = _pos + Vector3.back * Time.deltaTime * SheepSpeed;
        }
    }

    public void EndTime()
    {
        transform.position = new Vector3(-9, -253, 855);
    }
}