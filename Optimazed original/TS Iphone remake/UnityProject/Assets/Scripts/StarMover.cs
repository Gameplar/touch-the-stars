﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class StarMover : MonoBehaviour
{
    public float myTimer;
    public double blowTime;
    public float StarSetSpeed;


    private float direction1 = Random.Range(Sky.BlowSizeMin, Sky.BlowSizeMax);
    private float direction2 = Random.Range(Sky.BlowSizeMin, Sky.BlowSizeMax);
    private float dis = Random.Range(1, 30);
    public float dawnTime;
    private static Color myColor = Color.white;
    private Vector3 _pos;

   // private void Start()
  //  {
  //      myMaterial = gameObject.renderer.materials[1]; // взял нужный материал из прикрепленных к объекту
   // }

    private void Update()
    {
        myTimer += Time.deltaTime;
        StarSetSpeed = 2 * (20* Sky.GameTime/180);

        //var direction1 = Random.Range(-20, 20);
        //var direction2 = Random.Range(-20, 20);

        //var direction1 = new Random();
        //var direction2 = new Random();

       

        if (myTimer > blowTime)
        {
            _pos = transform.position;

           //transform.position = _pos + Vector3.down * Time.deltaTime * StarSetSpeed + Vector3.back * Time.deltaTime * StarSetSpeed;
            transform.position = _pos + new Vector3(0f, -1f, 0.5f) * Time.deltaTime * StarSetSpeed;
        }
        
        else if (myTimer < blowTime)
        {
            transform.Translate(new Vector3(direction1, direction2, 0)*Time.deltaTime*4);
        }

       
        if (Sky.GameTime > Sky.nightTime + dis)
        {
            Destroy(gameObject);
        }
        
        if (transform.position.y < -220)
        {
            Destroy(gameObject);
        }

        // if (Sky.GameTime > dawnTime)
        // {
        //     myMixer = (dawnDuration - (Sky.GameTime - dawnTime)) / dawnDuration;
        //     myColor.a = myMixer; // альфа канал цвета теперь зависит от myMixer
        //     myMaterial.SetColor("_Color", myColor);
        // }
    }
}