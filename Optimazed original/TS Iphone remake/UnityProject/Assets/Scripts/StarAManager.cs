﻿using System;
using System.Threading;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;


public sealed class StarAManager : MonoBehaviour
{
    public GameObject prefabA;
    public GameObject prefabB;
    public GameObject prefabA3;
    public Int32 radius;
    public Int32 radiusB;
    public Int32 starBNum;
    public Int32 starA3Num;
    public float StarLifeTime;

    public Single[] StarATiming = new Single[99];
    public Single[] StarBTiming = new Single[19];
    public Single[] StarA3Timing = new Single[19];

    public static Int32 curStarA = 0;
    public static Int32 curStarB = 0;
    public static Int32 curStarA3 = 0;

    private void Update()
    {
        var direction1 = Random.Range(50, 400);
        var direction2 = Random.Range(-500, 500);


        //foreach (Single time in StarATiming)
        //{
        //    if (Sky.GameTime > time)
        //    {
        //        Instantiate(prefabA, new Vector3(-100, direction1, direction2), transform.rotation);
        //        curStarA = curStarA + 1;
        //    }
        //}

        for (int i = curStarA; i < StarATiming.Length; i++)
        {
            if (Sky.GameTime > StarATiming[i])
            {
                Instantiate(prefabA, new Vector3(-100, direction1, direction2), transform.rotation);
                curStarA = curStarA + 1;
            }
        }

        for (int y = curStarB; y < StarBTiming.Length; y++)
        {
            if (Sky.GameTime > StarBTiming[y])
            {

                for (int a = 0; a < starBNum; a++)
                {
                    Instantiate(prefabB, new Vector3(-100, direction1, direction2), transform.rotation);
                }
                curStarB = curStarB + 1;
            }
        }
        
        for (int x = curStarA3; x < StarA3Timing.Length; x++)
        {
            if (Sky.GameTime > StarA3Timing[x])
            {
   
                for (int k = 0; k < starA3Num; k++)
                {
                    var direction3 = Random.Range(-500, 500);
                    Instantiate(prefabA3, new Vector3(-100, 505, direction3), transform.rotation);
                }
                curStarA3 = curStarA3 + 1;
            }
        }

    }
}