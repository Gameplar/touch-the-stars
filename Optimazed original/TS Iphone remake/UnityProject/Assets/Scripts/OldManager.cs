﻿using UnityEngine;
using System.Collections;

public class OldManager : MonoBehaviour
{

    public float myMixer = 0;
    private static Color myColor = Color.white;
    private static Material myMaterial;
    public int ChangetFlag;
    public float ChangeTimer;
    public float ChangeTime;
    private int x;
    private int y;
    public int AppearFlag;
    public int HideFlag;
    public float AppearTime;
    public float HideTime;
    public LanternLight Light;

    private void Start()
    {
        myMaterial = gameObject.GetComponent<Renderer>().materials[0];
        myColor.a = myMixer;
        myMixer = 0;
        myMaterial.SetColor("_Color", myColor);
    }

    private void Update()
    {
        if (AppearFlag == 0)
        {
            if (Sky.GameTime > AppearTime)
            {
                appear();
                AppearFlag = 1;
            }
        }
        if (HideFlag == 0)
        {
            if (Sky.GameTime > HideTime)
            {
                hide();
                Light.hide();
                HideFlag = 1;
            }
        }

        if (ChangetFlag == 1)
        {
            ChangeTimer += Time.deltaTime;

            if (ChangeTimer < ChangeTime)
            {
                myMixer = (x * ChangeTime + y * ChangeTimer) / ChangeTime;
                myColor.a = myMixer;
                myMaterial.SetColor("_Color", myColor);
            }
            else if (ChangeTimer > ChangeTime)
            {
                ChangetFlag = 0;
                ChangeTimer = 0;
            }
        }
    }

    public void appear()
    {
        ChangetFlag = 1;
        x = 0;
        y = 1;
    }

    public void hide()
    {
        ChangetFlag = 1;
        x = 1;
        y = -1;
    }

}
