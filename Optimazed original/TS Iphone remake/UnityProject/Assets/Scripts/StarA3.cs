﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class StarA3 : MonoBehaviour
{
    private float direction1;
    private float direction2;
    public float StarLifeTime;
    public double fallTime;
    public float myTimer;
    public GameObject[] starlist = new GameObject[10];
    public Int32 starnum;

    private void Awake()
    {
        direction1 = Random.Range(-50, 50);
        direction2 = Random.Range(-50, 50);
    } 
    
    
    private void Update()
    {
        myTimer += Time.deltaTime;

        if (myTimer < fallTime)
        {
            transform.Translate(new Vector3(direction1, -50, 0) * Time.deltaTime * 3);
        }

        if (transform.position.y < -220)
        {
            Destroy(gameObject);
        }

        if (myTimer > StarLifeTime)
        {
            Destroy(gameObject);
        }
    }

    private void OnMouseOver()
    {
        var random = new Random();

        for (int i = 1; i < starnum; i++)
        {
            int number = Random.Range(0, 9);


            Instantiate(starlist[number], transform.position, transform.rotation);
        }

        Destroy(gameObject);
    }
}