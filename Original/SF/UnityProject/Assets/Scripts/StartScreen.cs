﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour {

    public float myTimer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        
        myTimer += Time.deltaTime;
	    if (myTimer > 5)
	    {
            Destroy(gameObject);
  
	    }
	
	}

    void OnMouseDown()
    {
        Destroy(gameObject); 
    }
}
