﻿using UnityEngine;
using System.Collections;

public class OldManager : MonoBehaviour
{

    public float myMixer = 0;
    private static Color myColor = Color.white;
    private static Material myMaterial;
    // Use this for initialization
    void Start()
    {
        myMaterial = gameObject.renderer.materials[0]; // взял нужный материал из прикрепленных к объекту
    }

    // Update is called once per frame
    void Update()
    {
        myColor.a = myMixer; // альфа канал цвета теперь зависит от myMixer
        myMaterial.SetColor("_Color", myColor);
    }
    //public void Hide()
    //{
    //    transform.position = new Vector3(20, 0, 0);
    //}
    //public void Appear()
    //{
    //    transform.position = new Vector3(-20, -370, 0);
    //}

}
