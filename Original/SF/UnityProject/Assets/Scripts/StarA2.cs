﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class StarA2 : MonoBehaviour
{
    private float direction1 = Random.Range(-50, 50);
    private float direction2 = Random.Range(-50, 50);
    public float StarLifeTime;
    public double blowTime;
    public float myTimer;
    public GameObject[] starlist = new GameObject[10];
    public Int32 starnum;

    private void Update()
    {
        myTimer += Time.deltaTime;

        if (myTimer < blowTime)
        {
            transform.Translate(new Vector3(direction1, 0, direction2)*Time.deltaTime*4);
        }


        if (myTimer > StarLifeTime)
        {
            Destroy(gameObject);
        }
    }

    private void OnMouseOver()
    {
        var random = new Random();

        for (int i = 1; i < starnum; i++)
        {
            int number = Random.Range(0, 9);


            Instantiate(starlist[number], transform.position, transform.rotation);
        }

        Destroy(gameObject);
    }
}