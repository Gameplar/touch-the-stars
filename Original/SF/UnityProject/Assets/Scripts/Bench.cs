﻿using UnityEngine;
using System.Collections;

public class Bench : MonoBehaviour
{
    public float KidsAppear;
    public float KidsHide;
    public float AdultAppear;
    public float AdultHide;
    public float OldAppear;
    public float OldHide;
    public float ApperTime;
    private static float curHero = 0;
    public KidsManager kids;
    public AdultManager adults;
    public OldManager olds;
    public Baloons baloons;

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (Sky.GameTime > KidsAppear)
        {
            if (Sky.GameTime <= AdultAppear)
            {
                if (Sky.GameTime < KidsAppear + ApperTime)
                {
                    kids.myMixer = (Sky.GameTime - KidsAppear)/ApperTime;
                    baloons.myMixer = (Sky.GameTime - KidsAppear) / ApperTime;

                    //curHero = 1;
                    //kids.Appear();
                }
                if (Sky.GameTime > KidsAppear + ApperTime)
                {
                    kids.myMixer = 1;
                }
                if (Sky.GameTime > KidsHide)
                {
                    kids.myMixer = (AdultAppear - Sky.GameTime)/(AdultAppear - KidsHide);
                }
            }

            else
            {
                if (Sky.GameTime <= OldAppear)
                {
                    if (Sky.GameTime < AdultAppear + ApperTime)
                    {
                        adults.myMixer = (Sky.GameTime - AdultAppear)/ApperTime;

                        //curHero = 1;
                        //kids.Appear();
                    }
                    if (Sky.GameTime > AdultAppear + ApperTime)
                    {
                        adults.myMixer = 1;
                    }
                    if (Sky.GameTime > AdultHide)
                    {
                        adults.myMixer = (OldAppear - Sky.GameTime)/(OldAppear - AdultHide);
                    }
                }
                if (Sky.GameTime >= OldAppear)
                {
                    if (Sky.GameTime < OldAppear + ApperTime)
                    {
                        olds.myMixer = (Sky.GameTime - OldAppear) / ApperTime;

                        //curHero = 1;
                        //kids.Appear();
                    }
                    if (Sky.GameTime > OldAppear + ApperTime)
                    {
                        olds.myMixer = 1;
                    }
                    if (Sky.GameTime > OldHide)
                    {
                        olds.myMixer = (OldHide +5 - Sky.GameTime) / (5);
                    }
                }
            }

        }
        //if (Sky.GameTime > AdultAppear)
        //{
        //    if (Sky.GameTime < AdultAppear + ApperTime)
        //    {
        //        //curHero = 2;
        //        //kids.Hide();
        //        //adults.Appear();
        //    }
        //}
        //if (Sky.GameTime > OldAppear)
        //{
        //    if (Sky.GameTime < OldAppear + ApperTime)
        //    {
        //        //curHero = 3;
        //        //adults.Hide();
        //        //olds.Appear();
        //    }
        //}
        //if (Sky.GameTime > OldHide)
        //{
        //    if (curHero > 0)
        //    {
        //        //curHero = 0;
        //        //olds.Hide();
        //    }
        //}
    }
}