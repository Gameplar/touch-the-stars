﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class StarMover : MonoBehaviour
{
    public float dawnDuration;
    public float myMixer;
    public float myTimer;
    public double blowTime;
    public float StarSetSpeed;


    private float direction1 = Random.Range(Sky.BlowSizeMin, Sky.BlowSizeMax);
    private float direction2 = Random.Range(Sky.BlowSizeMin, Sky.BlowSizeMax);
    private float dis = Random.Range(1, 30);
    private static Material myMaterial;
    public float dawnTime;
    private static Color myColor = Color.white;

   // private void Start()
  //  {
  //      myMaterial = gameObject.renderer.materials[1]; // взял нужный материал из прикрепленных к объекту
   // }

    private void Update()
    {
        myTimer += Time.deltaTime;

        //var direction1 = Random.Range(-20, 20);
        //var direction2 = Random.Range(-20, 20);

        //var direction1 = new Random();
        //var direction2 = new Random();

        if (transform.position.y < -220)
        {
            Destroy(gameObject);
        }

        if (myTimer < blowTime)
        {
            transform.Translate(new Vector3(direction1, 0, direction2)*Time.deltaTime*4);
        }

        if (myTimer > blowTime)
        {
            var b = transform.position;

            transform.position = b + Vector3.down*Time.deltaTime*StarSetSpeed;
        }
        if (Sky.GameTime > Sky.nightTime + dis)
        {
            Destroy(gameObject);
        }

        // if (Sky.GameTime > dawnTime)
        // {
        //     myMixer = (dawnDuration - (Sky.GameTime - dawnTime)) / dawnDuration;
        //     myColor.a = myMixer; // альфа канал цвета теперь зависит от myMixer
        //     myMaterial.SetColor("_Color", myColor);
        // }
    }
}