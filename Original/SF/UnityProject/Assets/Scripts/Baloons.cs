﻿using UnityEngine;
using System.Collections;

public class Baloons : MonoBehaviour {

    public float myMixer;
    public float BaloonSpeed;
    public float BaloonFlyTime;
    public float BaloonHideTime;
    private static Color myColor = Color.white;
    private static Material myMaterial;

	// Use this for initialization
	void Start () {
        
        
        myMaterial = gameObject.renderer.materials[0]; // взял нужный материал из прикрепленных к объекту
	
	}
	
	// Update is called once per frame
	void Update () {
        myColor.a = myMixer; // альфа канал цвета теперь зависит от myMixer
        myMaterial.SetColor("_Color", myColor);

        if (Sky.GameTime > BaloonFlyTime)
        {
            var b = transform.position;
            transform.position = b + Vector3.up * Time.deltaTime * BaloonSpeed;
            myMixer = (BaloonHideTime - Sky.GameTime) / (BaloonHideTime - BaloonFlyTime);
        }
	
	}
    public void EndTime()
    {
        transform.position = new Vector3(-162, -300, 125);
    }
}
