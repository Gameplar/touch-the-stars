﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class StarA2 : MonoBehaviour
{
    private float direction1 = Random.Range(-50, 50);
    private float direction2 = Random.Range(-50, 50);
    public float StarLifeTime;
    public double blowTime;
    public float myTimer;
    public GameObject[] starlist = new GameObject[10];
    public Int32 starnum;
    private Vector3 _pos;
    public GameObject BlackStar;
    private Transform _transform;

    	void Start ()
	{
        _transform = transform;
    }

    private void Update()
    {
        myTimer += Time.deltaTime;

        if (myTimer < blowTime)
        {
            //transform.Translate(new Vector3(direction1, direction2, 0) * Time.deltaTime * 7);
            _pos = _transform.position;
            _transform.position = _pos + new Vector3(0f, -50f, direction1) * Time.deltaTime * 4;
        }


        if (myTimer > StarLifeTime)
        {
            Destroy(gameObject);
            //Instantiate(BlackStar, transform.position, transform.rotation);
        }
        
        if (_transform.position.y < -220)
        {
            Destroy(gameObject);
            //Instantiate(BlackStar, transform.position, transform.rotation);
        }
    }

    private void OnMouseOver()
    {
        //var random = new Random();

        for (int i = 1; i < starnum; i++)
        {
            int number = Random.Range(0, 9);


            Instantiate(starlist[number], transform.position, transform.rotation);
        }

        Destroy(gameObject);
    }
}