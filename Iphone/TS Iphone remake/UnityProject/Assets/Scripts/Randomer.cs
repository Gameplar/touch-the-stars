﻿using UnityEngine;
using Random = UnityEngine.Random;


public class Randomer : MonoBehaviour
{
    public float dis;

    private void Awake()
    {
        dis = Random.Range(1, 10);
    }

    public float randomizer()
    {
        return dis;
    }
}