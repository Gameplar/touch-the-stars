﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class StarA3 : MonoBehaviour
{
    private float direction1;
    private float direction2;
    public float StarLifeTime;
    public double fallTime;
    public float myTimer;
    public GameObject[] starlist = new GameObject[10];
    public Int32 starnum;
    private Vector3 _pos;
    public GameObject BlackStar;
    private Transform _transform;
    private void Awake()
    {
        //direction1 = Random.Range(-50, 50);
        //direction2 = Random.Range(-50, 50);
    }

    void Start()
    {
        _transform = transform;
    }
    
    
    private void Update()
    {
        myTimer += Time.deltaTime;

        if (myTimer < fallTime)
        {
            //transform.Translate(new Vector3(0, -50, 25) * Time.deltaTime * 3);

            _pos = _transform.position;

            //transform.position = _pos + Vector3.down * Time.deltaTime * StarSetSpeed + Vector3.back * Time.deltaTime * StarSetSpeed;
            _transform.position = _pos + new Vector3(0f, -1f, 0.5f) * Time.deltaTime * 200;
        }

        if (_transform.position.y < -220)
        {
            Destroy(gameObject);
            //Instantiate(BlackStar, transform.position, transform.rotation);
        }

        if (myTimer > StarLifeTime)
        {
            Destroy(gameObject);
            //Instantiate(BlackStar, transform.position, transform.rotation);

        }
    }

    private void OnMouseOver()
    {
        //var random = new Random();

        for (int i = 1; i < starnum; i++)
        {
            int number = Random.Range(0, 9);


            Instantiate(starlist[number], transform.position, transform.rotation);
        }

        Destroy(gameObject);
    }
}