﻿using UnityEngine;
using System.Collections;

public class LeftPalm : MonoBehaviour {

    public int ScreenHight;
    public int ScreenWidth;

    private Vector3 _position;
    private float _z;
    private float _newZ;
    private float _coef;

    // Use this for initialization
    private void Start()
    {
        ScreenHight = Screen.height;
        ScreenWidth = Screen.width;

        _coef = ((float)ScreenHight) / ScreenWidth;

        _position = transform.position;
        _z = transform.position.z;
        _newZ = _z * (1.775f / (_coef));

        transform.position = _position - new Vector3(0, 0, _z) + new Vector3(0, 0, _newZ);

    }


}
