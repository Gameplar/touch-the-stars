﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour {

    public float myTimer;
    public StartText text;
    

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        
        myTimer += Time.deltaTime;
	    if (myTimer > 6)
	    {
            Destroy(gameObject);
            text.appear();
  
	    }
	
	}

    void OnMouseDown()
    {
        Destroy(gameObject); 
        text.appear();
    }
}
