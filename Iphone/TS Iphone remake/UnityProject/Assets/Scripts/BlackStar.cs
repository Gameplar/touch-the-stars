﻿using UnityEngine;
using System.Collections;

public class BlackStar : MonoBehaviour
{

    private float MyTimer;
    
    // Use this for initialization
	void Start ()
	{
	    Sky.VolumePenaltyTimer = 0.5f;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    MyTimer += Time.deltaTime;
	    if (MyTimer > 1)
	    {
            Destroy(gameObject); 
	    }

	}
}
