﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class StarterStar : MonoBehaviour
{
    private float direction1;
    private float direction2;
    public float StarLifeTime;
    public double blowTime;
    public float myTimer;
    public GameObject[] starlist = new GameObject[10];
    public Int32 starnum;
    public StartText text;
    public int ScreenHight;
    void Start()
    {

        ScreenHight = Screen.height;
        if (ScreenHight > 1000)
        {
            transform.position = new Vector3(-100f, 600f, -6f);
        }
        else
        {
            if (ScreenHight < 1000)
            {
                transform.position = new Vector3(-100f, 500f, -6f);
            }
        }

    }

    private void Awake()
    {
        direction1 = Random.Range(-50, 50);
        direction2 = Random.Range(-50, 50);
    }

    private void Update()
    {
        myTimer += Time.deltaTime;

        if (myTimer < blowTime)
        {
            transform.Translate(new Vector3(direction1, 0, direction2)*Time.deltaTime*4);
        }
    }

    public void OnMouseOver()
    {
        //var random = new Random();
        Sky.starter(1);
        text.hide();

        for (int i = 1; i < starnum; i++)
        {
            int number = Random.Range(0, 9);


            Instantiate(starlist[number], transform.position, transform.rotation);
        }
        //  StartText.hide();

        transform.position = new Vector3(20, -2850, -10);
    }

    public void Appear()
    {
        transform.position = new Vector3(-100, 520, -6);
    }
}