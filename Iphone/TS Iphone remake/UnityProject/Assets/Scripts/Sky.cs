﻿using System;
using UnityEngine;
using System.Collections;

public class Sky : MonoBehaviour
{
    #region editor

    public float myMixer;
    public float darkeningTime;
    public float startDarkeningTime = 200;
    public float dawnTime;
    public float dawnDuration;
    public float startSetPhase;


    public AudioClip myClip;
    private bool playAudio;
    //public GameObject StartStar;

    #endregion editor

    public static float GameTime;
    public float GameTimePublic;
    public static double nightTime = 150;
    public static float BlowSizeMin;
    public static float BlowSizeMax;
    public static float isStart = 0;

    private static Color myColor = Color.white;
    private static Material myMaterial;

    public SheepMover sheep;
    public StartText text;
    public StarterStar startstar;
    public Baloons baloons;
    public AdultManager Adults;
    public KidsManager Kids;
    public OldManager Olds;

    public static float VolumePenaltyTimer;

    public float AudioVol;
    public float VolumePenalty;


    public delegate void MyMethod();

    public MyMethod MethodInvoke;


    public static void starter(int s)
    {
        isStart = s;
    }


    private void Start()
    {
        myMaterial = gameObject.GetComponent<Renderer>().materials[0
]; // взял нужный материал из прикрепленных к объекту
        Application.targetFrameRate = 60;
    }

    private void Update()
    {
        // если в будущем использовать эту механику, то нужно выносить в отдельный метод
        //if (VolumePenaltyTimer > 0)
        //{
        //    VolumePenaltyTimer -= Time.deltaTime;
        //    audio.volume = VolumePenalty;
        //}
        //else
        //{
        //    audio.volume = AudioVol;
        //}


        if (isStart > 0)
        {
            GameTime += Time.deltaTime;
            GameTimePublic += Time.deltaTime;
            if (playAudio == false)
            {
                GetComponent<AudioSource>().PlayOneShot(myClip);
                playAudio = true;
            }
        }


        if (GameTime > 207)
        {
            isStart = 0;
            GameTime = 0;
            playAudio = false;
            StarAManager.curStarA = 0;
            StarAManager.curStarB = 0;
            StarAManager.curStarA3 = 0;

            startstar.Appear();


            sheep.EndTime();
            baloons.EndTime();
            text.appear();
            Adults.AppearFlag = 0;
            Adults.HideFlag = 0;
            Kids.AppearFlag = 0;
            Kids.HideFlag = 0;
            Olds.AppearFlag = 0;
            Olds.HideFlag = 0;
        }

        if (GameTime < 74)
        {
            BlowSizeMin = -50;
            BlowSizeMax = 50;
        }
        else
        {
            if (GameTime < 120)
            {
                BlowSizeMin = -75;
                BlowSizeMax = 75;
            }
            else
            {
                if (GameTime > 125)
                {
                    BlowSizeMin = -100;
                    BlowSizeMax = 100;
                }
            }
        }

        // темнеет, слой ночи становится все менее прозрачным
        if (GameTime < darkeningTime)
        {
            //myMixer = startSetPhase + (GameTime/darkeningTime)*startSetPhase;
            myMixer =  (GameTime / darkeningTime);

        }

        // ночь, темно, слой ночи непрозрачен
        if (GameTime > darkeningTime)
        {
            if (GameTime < dawnTime)
            {
                myMixer = 1;
            }

            // начинается рассвет, материал ночи постепенно становится прозрачнее
            if (GameTime > dawnTime)
            {
                if (GameTime < dawnTime + dawnDuration)
                {
                    myMixer = (dawnDuration - (GameTime - dawnTime))/dawnDuration;
                }

                // светло
                if (GameTime > dawnTime + dawnDuration)
                {
                    if (GameTime < startDarkeningTime)
                    {
                        myMixer = 0;
                    }

                    // темнеет
                    if (GameTime > startDarkeningTime)
                    {
                        myMixer = startSetPhase*(GameTime - startDarkeningTime)/(207 - startDarkeningTime);
                    }
                }
            }
        }

        //myMaterial.SetFloat("_Blend", myMixer);
        myMaterial.SetFloat("_Coefficient", myMixer);
        //myColor.a = myMixer; // альфа канал цвета теперь зависит от myMixer
        //myMaterial.SetColor("_Color", myColor);
    }
}