﻿using System;
using System.Threading;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;


public sealed class StarAManager : MonoBehaviour
{
    public GameObject prefabA;
    public GameObject prefabB;
    public GameObject prefabA3;
    public Int32 radius;
    public Int32 radiusB;
    public Int32 starBNum;
    public Int32 starA3Num;
    public float StarLifeTime;

    public Single[] StarATiming = new Single[99];
    public Single[] StarBTiming = new Single[19];
    public Single[] StarA3Timing = new Single[19];

    public static Int32 curStarA = 0;
    public static Int32 curStarB = 0;
    public static Int32 curStarA3 = 0;

    private float direction1;
    private float direction2;
    private float direction3;

    private int i;

    public int ScreenHight;

    //private void Start()
    //{
    //    ScreenHight = Screen.height;
    //    if (ScreenHight > 1000)
    //    {
    //        camera.orthographicSize = 708.2f;
    //        transform.position = new Vector3(-767.139f, 220f, 0f);
    //    }
    //    else
    //    {
    //        if (ScreenHight < 1000)
    //        {
    //            camera.orthographicSize = 620f;
    //            transform.position = new Vector3(-767.139f, 150f, 0f);
    //        }
    //    }
    //    Debug.Log(ScreenHight);
    //}

    private void Update()
    {
        //var direction1 = Random.Range(300, 800);
        //var direction2 = Random.Range(-300, 300);


        //foreach (Single time in StarATiming)
        //{
        //    if (Sky.GameTime > time)
        //    {
        //        Instantiate(prefabA, new Vector3(-100, direction1, direction2), transform.rotation);
        //        curStarA = curStarA + 1;
        //    }
        //}

        if (Sky.GameTime > 123)
        {
            for (int i = curStarA3; i < StarA3Timing.Length; i++)
            {
                if (Sky.GameTime > StarA3Timing[i])
                {
                    for (int k = 0; k < starA3Num; k++)
                    {
                        direction1 = Random.Range(550, 800);
                        direction2 = Random.Range(-300, 300);
                        Instantiate(prefabA3, new Vector3(-100, direction1, direction2), transform.rotation);
                    }
                    curStarA3 = curStarA3 + 1;
                }
            }
        }

        else if (Sky.GameTime > 72)
        {
            for (i = curStarB; i < StarBTiming.Length; i++)
            {
                if (Sky.GameTime > StarBTiming[i])
                {
                    for (int a = 0; a < starBNum; a++)
                    {
                        direction1 = Random.Range(600, 900);
                        direction2 = Random.Range(-300, 300);
                        Instantiate(prefabB, new Vector3(-100, direction1, direction2), transform.rotation);
                    }
                    curStarB = curStarB + 1;
                }
            }
        }

        else if (Sky.GameTime > 0)
        {
            for (i = curStarA; i < StarATiming.Length; i++)
            {
                if (Sky.GameTime > StarATiming[i])
                {
                    direction1 = Random.Range(300, 800);
                    direction2 = Random.Range(-300, 300);
                    Instantiate(prefabA, new Vector3(-100, direction1, direction2), transform.rotation);
                    curStarA = curStarA + 1;
                }
            }
        }
    }
}