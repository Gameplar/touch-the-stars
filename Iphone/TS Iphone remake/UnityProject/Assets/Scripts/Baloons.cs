﻿using UnityEngine;
using System.Collections;

public class Baloons : MonoBehaviour {

    public float myMixer;
    public float BaloonSpeed;
    public float BaloonFlyTime;
    public float BaloonHideTime;
    private static Color myColor = Color.white;
    private static Material myMaterial;
    private Vector3 _pos;
    private Transform _transform;


    public int ChangetFlag;
    public float ChangeTimer;
    public float ChangeTime;
    private int x;
    private int y;
    public int AppearFlag;
    public int HideFlag;
    public float AppearTime;
    public float HideTime;


	// Use this for initialization
	void Start ()
	{   
        myMaterial = gameObject.GetComponent<Renderer>().materials[0];
        myColor.a = myMixer;
        myMixer = 0;
        myMaterial.SetColor("_Color", myColor);
        _transform = transform;
	
	}
	
	// Update is called once per frame
	void Update () {
        myColor.a = myMixer; // альфа канал цвета теперь зависит от myMixer
        myMaterial.SetColor("_Color", myColor);

        if (Sky.GameTime > BaloonFlyTime)
        {
            _pos = _transform.position;
            _transform.position = _pos + Vector3.up * Time.deltaTime * BaloonSpeed;
            myMixer = (BaloonHideTime - Sky.GameTime) / (BaloonHideTime - BaloonFlyTime);
        }

        if (AppearFlag == 0)
        {
            if (Sky.GameTime > AppearTime)
            {
                appear();
                AppearFlag = 1;
            }
        }

        if (ChangetFlag == 1)
        {
            ChangeTimer += Time.deltaTime;

            if (ChangeTimer < ChangeTime)
            {
                myMixer = (x * ChangeTime + y * ChangeTimer) / ChangeTime;
                myColor.a = myMixer;
                myMaterial.SetColor("_Color", myColor);
            }
            else if (ChangeTimer > ChangeTime)
            {
                ChangetFlag = 0;
                ChangeTimer = 0;
            }
        }
	
	}
    public void EndTime()
    {
        _transform.position = new Vector3(-162, -300, 125);
        ChangetFlag = 0;
        AppearFlag = 0;
    }
    public void appear()
    {
        ChangetFlag = 1;
        x = 0;
        y = 1;
    }
}
