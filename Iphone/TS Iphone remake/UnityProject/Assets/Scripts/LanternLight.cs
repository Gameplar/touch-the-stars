﻿using UnityEngine;
using System.Collections;

public class LanternLight : MonoBehaviour
{
    public int appearflag;
    public int blinkflag;
    public float appearTimer;
    public float blinkingTimer;
    public float offTimer;

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (appearflag == 1)
        {
            appearTimer = appearTimer + Time.deltaTime;

            //if (appearTimer >= 0.3)
            //{
            //    //hide();
            //    appearflag = 0;
            //    appearTimer = 0;
            //}

            if (appearTimer >= 0.2)
            {
                appear();
                appearflag = 0;
                appearTimer = 0;
            }

            else if (appearTimer >= 0.1)
            {
                hide();
            }
          
            
        }

        if (blinkflag == 1)
        {
            blinkingTimer = blinkingTimer + Time.deltaTime;

            //if (blinkingTimer >= 0.3)
            //{
            //    hide();
            //    appearflag = 0;
            //    appearTimer = 0;
            //}

            if (blinkingTimer >= 0.2)
            {
                appear();
                blinkingTimer = 0;
                blinkflag = 0;
            }

            else if (blinkingTimer >= 0.1)
            {
                hide();
            }


        }
    }

    public void appear()
    {
        transform.position = new Vector3(-80f, -218.7f, -27f);
        appearflag = 1;
    }


    public void hide()
    {
        transform.position = new Vector3(80f, -218.7f, -27f);
    }

    public void blinking()
    {
        blinkflag = 1;
    }
}