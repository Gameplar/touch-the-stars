﻿using System;
using System.Threading;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;


public class StarBlow : MonoBehaviour
{
    public GameObject[] starlist = new GameObject[10];

    public Int32 starnum;
    public float StarLifeTime;
    public float myTimer;
    public GameObject BlackStar;

    private void Update()
    {
        myTimer += Time.deltaTime;
        if (myTimer > StarLifeTime)
        {
            Destroy(gameObject);
            //Instantiate(BlackStar, transform.position, transform.rotation);
        }
    }

    private void OnMouseOver()
    {
        var random = new Random();

        for (int i = 1; i < starnum; i++)
        {
            var number = Random.Range(0, 9);


            Instantiate(starlist[number], transform.position, transform.rotation);
        }

        Destroy(gameObject);
    }
}