﻿Shader "Custom/Blend2Tex" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_AltTex ("Alternative (RGBA)", 2D) = "white" {}
		_Coefficient ("Coefficient", Range (0.0, 1.0)) = 1.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _MainTex;
			sampler2D _AltTex;
			
			float _Coefficient;

			struct VertexInput {
				float4 pos : POSITION;
				float2 tex : TEXCOORD0;
			};
			
			struct Vertex2Fragment {
				float4 pos : SV_POSITION;
				float2 tex : TEXCOORD0;
			};

			void vert( in VertexInput vin, out Vertex2Fragment v2f ) {
				v2f.pos = mul( UNITY_MATRIX_MVP, vin.pos );
				v2f.tex = vin.tex;
			}
			
			void frag( in Vertex2Fragment v2f, out float4 col : COLOR ) {
				fixed4 a = tex2D( _MainTex, v2f.tex );
				fixed4 b = tex2D( _AltTex, v2f.tex );
				
				col.rgb = lerp( a.rgb, b.rgb, _Coefficient * b.a );
				col.a = 1;
			}
			ENDCG
		}
	} 
}